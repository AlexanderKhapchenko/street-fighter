export function createElement({ tagName, className, attributes = {}, innerText, childArray = [] }) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean); // Include only not empty className values after the splitting
    element.classList.add(...classNames);
  }

	if (innerText) {
		element.innerText = innerText;
	}

	if (childArray) {
		childArray.forEach((item) => element.append(item))
	}


  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
