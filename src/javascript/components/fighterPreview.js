import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';


export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
	const childArray = fighter 
		? [createFighterImage(fighter), createFighterInfo(fighter)] 
		: [createFighterWaitingElement()];
		
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
		childArray
  });
	
  return fighterElement;
}

export function createFighterInfo({ name, health, attack, defense }) {
	const infoElement = createElement({tagName: 'div', className: 'fighter-preview___info'});
	const max = { ...fighterService.maxValues }

	infoElement.append(
		createElement({
			tagName: 'span', 
			className: 'name', 
			innerText: `${name}`
		}),
		createElement({
			tagName: 'span', 
			className: 'health', 
			innerText: `Health: ${health}`,
		}),
		createBar({
			width: `width: ${health / max.health * 100}%`
		}),
		createElement({
			tagName: 'span', 
			className: 'attack', 
			innerText: `Attack: ${attack}`
		}),
		createBar({
			width: `width: ${attack / max.attack * 100}%`
		}),
		createElement({
			tagName: 'span', 
			className: 'defense', 
			innerText: `Defense: ${defense}`
		}),
		createBar({
			width: `width: ${defense / max.defense * 100}%`
		})
	);

	return infoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterWaitingElement() {
	const waiter = createElement({
		tagName: 'div',
		className: 'fighter-waiting',
		innerText: 'Choose a second fighter to fight'
	})
	return waiter;
}

function createBar({ width }) {
	const bar = createElement({
		tagName: 'div',
		className: 'bar',
		attributes: {
			style: width
		}
	});

	const barContainer = createElement({
		tagName: 'div',
		className: 'bar-container',
		childArray: [bar]
	});

	return barContainer;
}