export function saveWins({ _id }) {
	if (localStorage.getItem(_id)) {
		const wins = Number(localStorage.getItem(_id));
		localStorage.setItem(_id, wins + 1);
	}
	else {
		localStorage.setItem(_id, 1);
	}
}

export function getWins({ _id }) {
	const wins = localStorage.getItem(_id);
	return wins ? wins : 0;
}