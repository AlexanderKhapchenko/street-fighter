import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
		const defaultParameters = {
			isAttack: false,
			isBlock: false,
			isCritical: true,
		}

		const playerOne = {
			...firstFighter,
			maxHealth: firstFighter.health,
			...defaultParameters
		}

		const playerTwo = {
			...secondFighter,
			maxHealth: secondFighter.health,
			...defaultParameters
		}

		let pressed = new Set();

		window.addEventListener('keydown', (event) => {
			pressed.add(event.code);
			switch (event.code) {
				case controls.PlayerOneAttack: 
					makeAttack({ attacker: playerOne, defender: playerTwo});
					break;
				case controls.PlayerTwoAttack: 
					makeAttack({ attacker: playerTwo, defender: playerOne });
					break;
				case controls.PlayerOneBlock:
					playerOne.isBlock = true;
					break;
				case controls.PlayerTwoBlock:
					playerTwo.isBlock = true;
					break;
			}
			
			checkCriticalHit({ 
				keyCombination: controls.PlayerOneCriticalHitCombination, 
				attacker: playerOne, 
				defender: playerTwo,
				pressed 
			});

			checkCriticalHit({ 
				keyCombination: controls.PlayerTwoCriticalHitCombination, 
				attacker: playerTwo, 
				defender: playerOne,
				pressed 
			});

			updateHealthBar({ leftFighter: playerOne, rightFighter: playerTwo});

			if (playerOne.health <= 0 || playerTwo.health <= 0) {
				const winner = playerOne.health <= 0 ? playerTwo : playerOne;
				resolve(winner);
				winScene({ leftFighter: playerOne, rightFighter: playerTwo});
			}
		});

		window.addEventListener('keyup', (event) => {
			pressed.delete(event.code);
			switch (event.code) {
				case controls.PlayerOneBlock: 
					playerOne.isBlock = false;
					break;
				case controls.PlayerTwoBlock: 
					playerTwo.isBlock = false;
					break;
				case controls.PlayerOneAttack:
					playerOne.isAttack = false;
					break; 	
				case controls.PlayerTwoAttack:
					playerTwo.isAttack = false;
					break; 	
			}
		});
  });
}

function makeAttack({ attacker, defender }) {
	if (!attacker.isAttack && !attacker.isBlock && !defender.isBlock) {
		attacker.isAttack = true;
		defender.health -= getDamage(attacker, defender);
	}
}

function updateHealthBar({ leftFighter, rightFighter }) {
	const	leftFighterIndicator = document.getElementById('left-fighter-indicator');
	const rightFighterIndicator = document.getElementById('right-fighter-indicator');

	leftFighterIndicator.style.width = getFormatedHealth(leftFighter);
	rightFighterIndicator.style.width = getFormatedHealth(rightFighter);
}

function getFormatedHealth({ health, maxHealth }) {
	return `${health < 0 ? 0 : (health / maxHealth * 100)}%`;
}

function checkCriticalHit({ keyCombination, attacker, defender, pressed }) {
	const rightСombination = keyCombination.every(code => pressed.has(code));

	if (attacker.isCritical && rightСombination) {
		defender.health -= getCriticalHit(attacker);
		attacker.isCritical = false;
		setTimeout(() => attacker.isCritical = true, 10000);
	}
}

function winScene({ leftFighter, rightFighter }) {
	const	leftFighterElement = document.querySelector('.arena___left-fighter');
	const rightFighterElement = document.querySelector('.arena___right-fighter');

	leftFighterElement.classList.add(dieOrWinClass(leftFighter));
	rightFighterElement.classList.add(dieOrWinClass(rightFighter));
}

function dieOrWinClass({ health }) {
	return health <= 0 ? 'die' : 'win'
}

export function getDamage(attacker, defender) {
	const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getCriticalHit({ attack }) {
  return 2 * attack;
}

export function getHitPower({ attack }) {
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower({ defense }) {
	const dodgeChance = Math.random() + 1;
	return defense * dodgeChance;
}
