import { showModal } from "./modal";
import { saveWins, getWins } from "../localStorage/localStorage";
import { createFighterImage } from "../fighterPreview";
import { createElement } from "../../helpers/domHelper";

export function showWinnerModal(fighter) {
	saveWins(fighter);
	
	const message = createElement({
		tagName: 'h2',
		innerText: `Victories with this character: ${getWins(fighter)}`
	});
	const bodyElement = createElement({
		tagName: 'div',
		className: 'modal-body',
		childArray: [message, createFighterImage(fighter)]
	});

  showModal({
		title: `Winner ${fighter.name}`,
		bodyElement,
		onClose: () => location.reload()
	})
}
