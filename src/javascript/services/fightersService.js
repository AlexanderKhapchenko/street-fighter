import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';
	static maxValues = {};
	
  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
			await this.saveMaximumValues(apiResult);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
		try {
      const endpoint = `details/fighter/${id}.json`;
      const result = await callApi(endpoint);
      return result;
    } catch ({message}) {
      throw new Error(message);
    }
	}

	async saveMaximumValues(apiResult) {
		try {
			const unresolvedFighters = apiResult.map(async (res) => await this.getFighterDetails(res._id));
			const fighters = await Promise.all(unresolvedFighters);

			const health = Math.max(...fighters.map(fighters => fighters.health));
			const attack = Math.max(...fighters.map(fighters => fighters.attack));
			const defense = Math.max(...fighters.map(fighters => fighters.defense));

			this.maxValues = { health, attack, defense };
    } catch (error) {
      throw error;
    }
	}
}

export const fighterService = new FighterService();
